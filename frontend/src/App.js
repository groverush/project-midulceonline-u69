import React, { Fragment } from 'react';
import CrearCuenta from './paginas/auth/CrearCuenta';
import Login from './paginas/auth/Login';
import Home from './paginas/Home';
import ProductosAdmin from './paginas/productos/ProductosAdmin';
import ProductosCrear from './paginas/productos/ProductosCrear'
import ProductosEditar from './paginas/productos/ProductosEditar'
import AdminClientes from './paginas/clientes/AdminClientes';
import CrearCliente from './paginas/clientes/CrearCliente'
import EditarCliente from './paginas/clientes/EditarCliente'
import AdminEmpleados from './paginas/empleados/AdminEmpleados';
import CrearEmpleado from './paginas/empleados/CrearEmpleado'
import EditarEmpleado from './paginas/empleados/EditarEmpleado'
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';


function App() {
  return (
    <Fragment>
      <Router>
        <Routes>
          <Route path="/" exact element={<Login />} />
          <Route path="/crear-cuenta" exact element={<CrearCuenta />} />
          <Route path="/home" exact element={<Home />} />
          <Route path="/productos-admin" exact element={<ProductosAdmin />} />
          <Route path="/productos-crear" exact element={<ProductosCrear />} />
          <Route path="/productos-editar/:idproducto" exact element={<ProductosEditar />} />
          <Route path="/admin-clientes" exact element={<AdminClientes />} />
          <Route path="/crear-cliente" exact element={<CrearCliente />} />
          <Route path="/editar-cliente/:idcliente" exact element={<EditarCliente />} />
          <Route path="/admin-empleados" exact element={<AdminEmpleados />} />
          <Route path="/crear-empleado" exact element={<CrearEmpleado />} />
          <Route path="/editar-empleado/:idempleado" exact element={<EditarEmpleado />} />
        </Routes>

      </Router>
    </Fragment>
  );
}

export default App;
