import React, { useState, useEffect } from 'react';
import ContentHeader from '../../componentes/ContentHeader';
import Footer from '../../componentes/Footer';
import Navbar from '../../componentes/Navbar';
import SidebarContainer from '../../componentes/SidebarContainer';
import { useNavigate, useParams } from 'react-router-dom';
import APIInvoke from '../../utils/APIInvoke'
import swal from 'sweetalert';

const EditarEmpleado = () => {

    const navigate = useNavigate();

    const { idempleado } = useParams();
    let arreglo = idempleado.split('@');

    const nombreEmpleado = arreglo[1];
    const cargoEmpleado = arreglo[2];
    const telefonoEmpleado = arreglo[3];
    const emailEmpleado = arreglo[4] + '@' + arreglo[5];






    const [empleado, setEmpleado] = useState({
        nombre: nombreEmpleado,
        cargo: cargoEmpleado,
        email: emailEmpleado,
        telefono: telefonoEmpleado,


    });

    const { nombre, cargo, email, telefono } = empleado;

    useEffect(() => {
        document.getElementById("nombre").focus();
    }, [])

    const onChange = (e) => {
        setEmpleado({
            ...empleado,
            [e.target.name]: e.target.value
        })
    }

    const editarEmpledo = async () => {
        let arreglo = idempleado.split('@');
        const idEmpleado = arreglo[0];


        const data = {
            nombre: empleado.nombre,
            cargo: empleado.cargo,
            email: empleado.email,
            telefono: empleado.telefono,


        }

        const response = await APIInvoke.invokePUT(`/api/empleados/${idEmpleado}`, data);
        const idEmpleadoEditado = response._id

        if (idEmpleadoEditado !== idEmpleado) {
            const msg = "El empleado no fue editado correctamente.";
            swal({
                title: 'Error',
                text: msg,
                icon: 'error',
                buttons: {
                    confirm: {
                        text: 'Ok',
                        value: true,
                        visible: true,
                        className: 'btn btn-danger',
                        closeModal: true
                    }
                }
            });

        } else {
            navigate("/admin-empleados");
            const msg = "El empleado fue editado correctamente.";
            swal({
                title: 'Información',
                text: msg,
                icon: 'success',
                buttons: {
                    confirm: {
                        text: 'Ok',
                        value: true,
                        visible: true,
                        className: 'btn btn-primary',
                        closeModal: true
                    }
                }
            });
        }
    }

    const onSubmit = (e) => {
        e.preventDefault();
        editarEmpledo();
    }

    return (
        <div className="wrapper">
            <Navbar></Navbar>
            <SidebarContainer></SidebarContainer>
            <div className="content-wrapper">

                <ContentHeader
                    titulo={"Edición de empleados"}
                    breadCrumb1={"Listado de empleados"}
                    breadCrumb2={"Edición"}
                    ruta1={"/admin-empleados"}
                />

                <section className="content">
                    <div className="card">
                        <div className="card-header">
                            <div className="card-tools">
                                <button type="button" className="btn btn-tool" data-card-widget="collapse" title="Collapse">
                                    <i className="fas fa-minus" />
                                </button>
                                <button type="button" className="btn btn-tool" data-card-widget="remove" title="Remove">
                                    <i className="fas fa-times" />
                                </button>
                            </div>
                        </div>
                        <div className="card-body">

                            <form onSubmit={onSubmit}>
                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">Nombre</label>
                                        <input type="text"
                                            className="form-control"
                                            id="nombre"
                                            name="nombre"
                                            placeholder="Ingrese el nombre del empleado"
                                            value={nombre}
                                            onChange={onChange}
                                            required
                                        />
                                    </div>
                                </div>
                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">Cargo</label>
                                        <input type="text"
                                            className="form-control"
                                            id="cargo"
                                            name="cargo"
                                            placeholder="Ingrese de la cargo"
                                            value={cargo}
                                            onChange={onChange}
                                            required
                                        />
                                    </div>
                                </div>


                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">Teléfono</label>
                                        <input type="text"
                                            className="form-control"
                                            id="telefono"
                                            name="telefono"
                                            placeholder="Ingrese de la telefono"
                                            value={telefono}
                                            onChange={onChange}
                                            required
                                        />
                                    </div>
                                </div>
                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">Email</label>
                                        <input type="email"
                                            className="form-control"
                                            id="email"
                                            name="email"
                                            placeholder="Ingrese de la email"
                                            value={email}
                                            onChange={onChange}
                                            required
                                        />
                                    </div>
                                </div>
                                <div className="card-footer">
                                    <button type="submit" className="btn btn-primary">Editar</button>
                                </div>
                            </form>

                        </div>
                    </div>
                </section>
            </div>
            <Footer></Footer>
        </div>
    );
}

export default EditarEmpleado;